package exercise4

import cats.data.State
import cats.implicits._

import scala.util.Random

object RNG {
  def between(min: Double, max: Double): State[Random, Double] = State(
    rng => (rng, rng.between(min, max))
  )

  def randomPair(min: Double, max: Double): State[Random, (Double, Double)] =
    for {
      a <- between(min, max)
      b <- between(min, max)
    } yield (a, b)

  def randomPairList(n: Int, min: Double, max: Double): State[Random, List[(Double, Double)]] =
    List.fill(n)(randomPair(min, max)).sequence
}
