package exercise4

import cats.Eval
import cats.data.State

import scala.language.postfixOps
import scala.math.sqrt
import scala.util.Random

object Pi {
  def pi(n: Int): Eval[Double] = Eval.later {
    val riemann2 = (1 to n) map {
      e => 1.0/(e*e)
    } sum

    sqrt(6 * riemann2)
  }

  def piMC(iterations: Int): State[Random, Double] =
    RNG.randomPairList(iterations, 0, 1) map { randPairList =>
      val totalInQuarterCircle = randPairList count { case (x, y) => ((x*x) + (y*y)) <= 1 }
      4.0 * (totalInQuarterCircle.toDouble / iterations)
    }

  def impurePiMC(iteration: Int): Double =
    piMC(iteration).runA(new Random()).value

}
