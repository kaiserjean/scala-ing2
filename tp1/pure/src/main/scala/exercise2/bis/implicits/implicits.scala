package exercise2.bis

import algebra.ring.Field

package object implicits {
  implicit class FieldOps[A: Field](x: A) {
    def one: A = Field[A].one
    def zero: A = Field[A].zero
    def unary_-(): A = Field[A].negate(x)
    def unary_+(): A = x
    def inverse(): A = Field[A].reciprocal(x)
    def +(rhs: A): A = Field[A].plus(x, rhs)
    def -(rhs: A): A = Field[A].plus(x, -rhs)
    def *(rhs: A): A = Field[A].times(x, rhs)
    def /(rhs: A): A = Field[A].div(x, rhs)
    def %(rhs: A): A = Field[A].emod(x, rhs)
    def |(rhs: A): A = Field[A].equot(x, rhs)
    def |%(rhs: A): (A, A) = Field[A].equotmod(x, rhs)
    def lcm(rhs: A)(implicit ev: algebra.Eq[A]): A = Field[A].lcm(x, rhs)
    def gcd(rhs: A)(implicit ev: algebra.Eq[A]): A = Field[A].gcd(x, rhs)
  }
}
