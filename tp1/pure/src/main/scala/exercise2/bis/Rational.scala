package exercise2.bis

import algebra.ring.Field

import scala.language.implicitConversions
import scala.math.Ordered.orderingToOrdered
import scala.math.abs

case class Rational(num: Int, den: Int)

object Rational {
  def apply(num: Int, den: Int = 1): Rational = den match {
    case 0 => INF
    case d if d < 0 => new Rational(-num, abs(d))
    case _ => new Rational(num, den)
  }

  val INF: Rational = new Rational(1, 0) {
    override def toString: String = "Infinity"
  }

  implicit val rationalField: Field[Rational] = new Field[Rational] {
    def one: Rational = Rational(1)
    def times(x: Rational, y: Rational): Rational = Rational(x.num * y.num, x.den * y.den)
    def negate(x: Rational): Rational = Rational(-x.num, x.den)
    def zero: Rational = Rational(0)
    def plus(x: Rational, y: Rational): Rational = Rational(
      (x.num * y.den) + (y.num * x.den),
      x.den * y.den
    )
    def div(x: Rational, y: Rational): Rational = Rational(x.num * y.den, x.den * y.num)
  }

  implicit val rationalComparable: Ordering[Rational] =
    (x: Rational, y: Rational) => Ordering.Int.compare(x.num * y.den, x.den * y.num)

  implicit def intToRational(x: Int): Rational = Rational(x)
}
