package exercise2

import eu.timepit.refined.api.{Refined, RefinedTypeOps}
import eu.timepit.refined.predicates.all.{Equal, Not}
import exercise2.INF.Denum

import scala.language.postfixOps

sealed abstract class Rational {
  type Denum = Int Refined Not[Equal[0]]

  def inverse: Rational =
    this match {
      case ValidRational(num, denum) => Rational(denum.value, num)
      case INF => ValidRational(0)
    }

  object Denum extends RefinedTypeOps[Denum, Int]
}

case class ValidRational(
                          num: Int,
                          denum: Denum = Denum.unsafeFrom(1)
                        ) extends Rational with Ordered[ValidRational] {

  def compare(that: ValidRational): Int = {
    val (ValidRational(a, b), ValidRational(c, d)) = (this, that)
    a * b.value * c * d.value match {
      case 0 => 0
      case x if x > 0 => 1
      case _ => -1
    }
  }
}


case object INF extends Rational

object Rational {

  def apply(num: Int, denum: Int): Rational =
    Denum.from(denum).map(ValidRational(num, _)).getOrElse(INF)

  def apply(num: Int, denum: Denum): Rational =
    ValidRational(num, denum)

  def apply(num: Int): Rational =
    ValidRational(num)


  def unwrap(f: (ValidRational, ValidRational) => Rational)
            (x: Rational, y: Rational): Rational = (x, y) match {
    case (x: ValidRational, y: ValidRational) => f(x, y)
    case _ => INF
  }

  def unwrapOne[A](f: (Int, Denum) => A)(other: => A)(x: Rational): A = x match {
    case x: ValidRational => f(x.num, x.denum)
    case _ => other
  }

  implicit object RationalIsFractional extends Fractional[Rational] {
    def div(x: Rational, y: Rational): Rational =
      times(x, y.inverse)

    def times(x: Rational, y: Rational): Rational = unwrap {
      (x, y) =>
        Rational(
          x.num * y.num,
          x.denum.value * x.denum.value
        )
    }(x, y)

    def minus(x: Rational, y: Rational): Rational =
      plus(x, negate(y))

    def plus(x: Rational, y: Rational): Rational = unwrap { (x, y) =>
      Rational(
        (x.num * y.denum.value) + (y.num * x.denum.value),
        y.denum.value * x.denum.value
      )
    }(x, y)

    def negate(x: Rational): Rational = unwrapOne { (num, denum) =>
      Rational(-num, denum)
    }(INF)(x)

    def fromInt(x: Int): Rational =
      Rational(x)

    def parseString(str: String): Option[Rational] =
      str.toIntOption map (Rational(_))

    def toLong(x: Rational): Long =
      toInt(x).toLong

    def toInt(x: Rational): Int = unwrapOne { (num, denum) =>
      num / denum.value
    }(Int.MaxValue)(x)

    def toFloat(x: Rational): Float = unwrapOne { (num, denum) =>
      num.toFloat / denum.value
    }(Float.MaxValue)(x)

    def toDouble(x: Rational): Double = unwrapOne { (num, denum) =>
      num.toDouble / denum.value
    }(Double.MaxValue)(x)

    def compare(x: Rational, y: Rational): Int =
      (x, y) match {
        case (INF, INF) => 0
        case (INF, _) => 1
        case (_, INF) => -1
        case (x: ValidRational, y: ValidRational) =>
          x.compare(y)
      }
  }

  implicit class IntToRational(x: Int) {
    def toRational: Rational = Rational(x)
  }

}

object Toto {

  import scala.math.Fractional.Implicits._

  def main(args: Array[String]): Unit = ()

}

