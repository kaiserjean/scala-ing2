name := "pure"

version := "0.1"

scalaVersion := "2.13.7"

libraryDependencies ++= Seq(
  "eu.timepit"    %% "refined"      % "0.9.28",
  "org.typelevel" %% "algebra"      % "2.7.0",
  "org.typelevel" %% "algebra-laws" % "2.7.0",
  "org.typelevel" %% "cats-core"    % "2.7.0"
)