package exercise1

object Calendar {
  def isLeap(year: Int): Option[Boolean] =
    if(year < 0) None
    else Some(
      (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
    )

  def numberOfDays(leap: Boolean, month: Int): Option[Int] = {
    month match {
      case 1 | 3 | 5 | 7 | 8 | 10 | 12 => Some(31)
      case 4 | 6 | 9 | 11              => Some(30)
      case 2 if leap                   => Some(29)
      case 2                           => Some(28)
      case _                           => None
    }
  }

  //numberOfDays could be improved by just asking for year and month and deducing if the year is leap inside
  def numberOfDaysImproved(year: Int, month: Int): Option[Int] =
    //isLeap renvoie une Option donc il faut déconstruire pour récupérer le booléen dans Some
    isLeap(year) match {
      case Some(leap) => numberOfDays(leap, month) //si la valeur est un Some(Boolean) on appelle numberOfDays qui renverra soit Some(Int) soit None
      case None => None //si la valeur est none on renvoie None directement
    }
}
