package exercise3

import exercise2.{Rational2 => Rational}

sealed trait Expression
case class Const(value: Rational) extends Expression
case class Negate(a: Expression) extends Expression
case class Add(a: Expression, b: Expression) extends Expression
case class Sub(a: Expression, b: Expression) extends Expression
case class Times(a: Expression, b: Expression) extends Expression

object Expression {
  def +(a: Expression, b: Expression): Expression =
    Add(a, b)

  def -(a: Expression, b: Expression): Expression =
    Sub(a, b)

  def *(a: Expression, b: Expression): Expression =
    Times(a, b)

  def unary_-(a: Expression): Expression =
    Negate(a)

  def apply(r: Rational): Expression =
    Const(r)

  def eval(expression: Expression): Rational = expression match {
    case Const(value) => value
    case Add(a, b) => eval(a) + eval(b)
    case Sub(a, b) => eval(a) - eval(b)
    case Times(a, b) => eval(a) * eval(b)
    case Negate(a) => -eval(a)
  }
}