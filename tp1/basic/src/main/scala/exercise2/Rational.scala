package exercise2

case class Rational(num: Int, denum: Int) extends Ordered[Rational] {
  def unary_- : Rational =
    Rational(-this.num, this.denum)

  def +(that: Rational): Rational =
    Rational(this.num+that.num, this.denum*that.denum)

  def -(that: Rational): Rational =
    this + (-that)

  def *(that: Rational): Rational =
    Rational(this.num*that.num, this.denum*that.denum)

  def /(that: Rational): Rational =
    Rational(this.num*that.denum, this.denum*that.num)

  override def compare(that: Rational): Int = {
    val diff = this - that
    diff.num*diff.denum
  }

}

object Rational {
  val ZERO: Rational = Rational(0)
  val ONE: Rational = Rational(1)
  val INF: Rational = Rational(1, 0)

  def apply(num: Int, denum: Int = 1): Rational =
    if(denum == 0) INF
    else new Rational(num, denum)
}

case class Rational2(num: Int, denum: Int = 1) extends Ordered[Rational2] {
  def unary_- : Rational2 =
    Rational2(-this.num, this.denum)

  def +(that: Rational2): Rational2 =
    Rational2(this.num+that.num, this.denum*that.denum)

  def -(that: Rational2): Rational2 =
    this + (-that)

  def *(that: Rational2): Rational2 =
    Rational2(this.num*that.num, this.denum*that.denum)

  def /(that: Rational2): Rational2 =
    Rational2(this.num*that.denum, this.denum*that.num)

  override def compare(that: Rational2): Int = {
    Ordering.Int.compare(this.num*that.denum, this.denum*that.num)
  }
}

object Rational2 {
  /**
      /!\ ULTRA MOCHE !!!! CECI EST DANGEREUX ET REALISE PAR DES PROFESSIONEL, NE PAS REPRODUIRE CHEZ SOI /!\

      arendsyl corp ne peut se voir pursuivie pour tout problème suite à l'utilisation de cette technique en production
  **/
  import scala.language.implicitConversions

  implicit def toRational(x: Int): Rational2 = {
    Rational2(x)
  }
}



