package exercise4

import scala.annotation.tailrec
import scala.util.Random

object Pi {
  def pi(n : Int): Double = {
    @tailrec def pi(n: Int)(acc: Double): Double =
      if(n == 1) acc
      else pi(n-1)(acc+(1/n*n))

    pi(n)(0.0)
  }


  def piMC(n: Int): Double = {
    @tailrec def piMC(n: Int)(iterations: Int, inCircle: Int): Double =
      if(n == 0) 4 * inCircle.toDouble / iterations
      else {
        val drawInCircle = {
          val (x, y) = (Random.nextDouble(), Random.nextDouble())
          if(x*x + y*y <= 1) 1
          else 0
        }
        piMC(n-1)(
          iterations,
          inCircle + drawInCircle
        )
      }

    piMC(n)(n, 0)
  }

}
