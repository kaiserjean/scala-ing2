package exercise4

import scala.language.postfixOps
import scala.math.sqrt
import scala.util.Random

object Pi {

  def pi(n: Int): Double = {
    val l = (0 to n) map { elem =>
      1/elem*elem
    } sum

    sqrt(6 * l)
  }

  def piMC(n: Int): Double = {
    val nextDoublePair = (Random.nextDouble, Random.nextDouble)
    val qtyPointsInQuarterCircle = List.fill(n)(nextDoublePair) count {
      case (x, y) => x*x + y*y <= 1
    }
    4 * (qtyPointsInQuarterCircle.toDouble / n)
  }
}
