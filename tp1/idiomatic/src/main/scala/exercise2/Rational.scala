package exercise2


import scala.language.{implicitConversions, postfixOps}
import scala.math.abs

case class Rational(num: Int, denum: Int) {
  override def toString: String =
    if (this.denum == 1) num.toString
    else s"$num / $denum"

  def inverse: Rational =
    Rational(denum, num)
}

object Rational {
  val INF: Rational = new Rational(1, 0) {
    override def toString: String = "Infinity"
  }

  def apply(num: Int, denum: Int = 1): Rational =
    denum match {
      case 0 => Rational.INF
      case x if x < 0 => new Rational(-num, abs(denum))
      case _ => new Rational(num, denum)
    }

  implicit object RationalIsFractional extends Fractional[Rational] {
    implicit def toRational(x: Int): Rational = Rational(x)

    def div(x: Rational, y: Rational): Rational =
      Rational(x.num * y.denum, y.num * x.denum)

    def plus(x: Rational, y: Rational): Rational =
      Rational((x.num * y.denum) + (y.num * x.denum), x.denum * y.denum)

    def minus(x: Rational, y: Rational): Rational =
      plus(x, negate(y))

    def times(x: Rational, y: Rational): Rational =
      Rational(x.num * y.num, x.denum * y.denum)

    def negate(x: Rational): Rational =
      Rational(-x.num, x.denum)

    def fromInt(x: Int): Rational =
      Rational(x)

    def parseString(str: String): Option[Rational] =
      str.toIntOption map (Rational(_))

    def toInt(x: Rational): Int =
      x.num / x.denum

    def toLong(x: Rational): Long =
      toInt(x).toLong

    def toFloat(x: Rational): Float =
      x.num.toFloat / x.denum

    def toDouble(x: Rational): Double =
      x.num.toDouble / x.denum

    def compare(x: Rational, y: Rational): Int =
      Ordering.Int.compare(x.num * y.denum, x.denum * y.num)
  }
}

object Toto {
  import exercise2.Rational.RationalIsFractional._
  import scala.math.Fractional.Implicits._
  def main(args: Array[String]): Unit = {
    val toto = Rational(0, 3)
    println(toto == Rational(0))
  }
}
