package exercise1

object Calendar {
  def isLeap(year: Int): Option[Boolean] =
    Option.when(year > 0) {
      (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
    }

  def numberOfDays(leap: Boolean, n: Int): Option[Int] =
    Some(n) collect {
      case 1 | 3 | 5 | 7 | 8 | 10 | 12 => 31
      case 4 | 6 | 9 | 11 => 30
      case 2 if leap => 29
      case 2 => 28
    }

  def numberOfDaysImproved(year: Int, month: Int): Option[Int] =
    isLeap(year) flatMap (numberOfDays(_, month))

  def numberOfDaysImprovedBis(year: Int, month: Int): Option[Int] =
    for {
      isLeap <- isLeap(year)
      days <- numberOfDays(isLeap, month)
    } yield days
}
